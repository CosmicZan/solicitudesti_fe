-- Create DataBase
create database SolicitudesTIStage;

-- Use DataBase
use SolicitudesTIStage;

-- Create Tables

-- Catalog EstadoEquipo
create table CatalogoEstadoEquipo (
	IdCatalogoEstadoEquipo int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoEstadoEquipo primary key(IdCatalogoEstadoEquipo)
);

select * from CatalogoEstadoEquipo;
drop table CatalogoEstadoEquipo;

-- Insert EstadoEquipo
insert into CatalogoEstadoEquipo (Nombre, Descripcion, Estatus) values 
('Nuevo', 'Select Estado del Equipo', '1'), 
('Usado', 'Select Estado del Equipo', '1');

select * from CatalogoEstadoEquipo;

drop table CatalogoEstadoEquipo;

-- Catalog TipoAsignacion
create table CatalogoTipoAsignacion (
	IdTipoAsignacion int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdTipoAsignacion primary key(IdTipoAsignacion)
);

-- Insert TipoAsignacion
insert into CatalogoTipoAsignacion (Nombre, Descripcion, Estatus) values 
('Nueva posici�n', 'Select Tipo de Asignacion', '1'), 
('Re-asignaci�n', 'Select Tipo de Asignacion', '1'),
('Cambio de equipo', 'Select Tipo de Asignacion', '1');

select * from CatalogoTipoAsignacion;

-- Catalog Area
create table CatalogoArea (
	IdArea int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdArea primary key(IdArea)
);

-- Insert Area
insert into CatalogoArea (Nombre, Descripcion, Estatus) values 
('Contraloria', 'Select de Area','1'),
('Direcci�n General', 'Select de Ubicacion', '1'),
('R.B.A Camarones', 'Select de Area','1'),
('R.B.A Zapata', 'Select de Area','1'),
('R.B.A Matriz', 'Select de Area','1');

select * from CatalogoArea;

-- Catalog Ubicacion
create table CatalogoUbicacion (
	IdCatalogoUbicacion int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoUbicacion primary key(IdCatalogoUbicacion)
);

select * from CatalogoUbicacion;

-- Insert Ubicacion
insert into CatalogoUbicacion (Nombre, Descripcion, Estatus) values 
('Aguas Calientes', 'Select de Ubicacion', '1'),
('Baja California', 'Select de Ubicacion', '1'),
('Chihuahua', 'Select de Ubicacion', '1'),
('Ciudad Obreg�n', 'Select de Ubicacion', '1'),
('Coahuila', 'Select de Ubicacion', '1'),
('Culiac�n', 'Select de Ubicacion', '1'),
('Guadalajara', 'Select de Ubicacion', '1'),
('Hermosillo', 'Select de Ubicacion', '1'),
('Le�n', 'Select de Ubicacion', '1'),
('Monterrey', 'Select de Ubicacion', '1'),
('Morelia', 'Select de Ubicacion', '1'),
('N/A', 'Select de Ubicacion', '1'),
('Puebla', 'Select de Ubicacion', '1'),
('Quer�taro', 'Select de Ubicacion', '1'),
('Saltillo', 'Select de Ubicacion', '1'),
('San Luis Potos�', 'Select de Ubicacion', '1'),
('Tabasco', 'Select de Ubicacion', '1'),
('Tijuana', 'Select de Ubicacion', '1'),
('Toluca', 'Select de Ubicacion', '1'),
('Veracruz', 'Select de Ubicacion', '1'),
('Yucat�n', 'Select de Ubicacion', '1'),
('Guatemala', 'Select de Ubicacion', '1'),
('El Salvador', 'Select de Ubicacion', '1'),
('Nicaragua', 'Select de Ubicacion', '1'),
('Honduras', 'Select de Ubicacion', '1'),
('Costa Rica', 'Select de Ubicacion', '1'),
('Panam�', 'Select de Ubicacion', '1'),
('Per�', 'Select de Ubicacion', '1'),
('Rep�blica Dominicana', 'Select de Ubicacion', '1'),
('Puerto Rico', 'Select de Ubicacion', '1');

select * from CatalogoUbicacion;

-- Catalog Cargo
create table CatalogoCargo (
	IdCatalogoCargo int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoCargo primary key(IdCatalogoCargo)
);

-- Insert Cargo
insert into CatalogoCargo (Nombre, Descripcion) values 
('', 'Select de ', '1');

select * from CatalogoCargo;

-- Catalog TipoMovimiento
create table CatalogoTipoMovimiento (
	IdCatalogoTipoMovimiento int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoTipoMovimiento primary key(IdCatalogoTipoMovimiento)
);

-- Insert TipoMovimiento
insert into CatalogoTipoMovimiento (Nombre, Descripcion, Estatus) values 
('Alta', 'Select de Tipo de Movimiento', '1'),
('Baja', 'Select de Tipo de Movimiento', '1'),
('Re-Direccionamiento', 'Select de Tipo de Movimiento', '1'),
('Cambio de Nombre', 'Select de Tipo de Movimiento', '1'),
('Cambio de Contrase�a', 'Select de Tipo de Movimiento', '1');

select * from CatalogoTipoMovimiento;
drop table CatalogoTipoMovimiento;

-- Catalog Dominio
create table CatalogoDominio (
	IdCatalogoDominio int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoDominio primary key(IdCatalogoDominio)
);

-- Insert Dominio
insert into CatalogoDominio (Nombre, Descripcion, Estatus) values 
('Prueba', 'Select de ', '1');

select * from CatalogoDominio;
drop table CatalogoDominio;

-- Catalog RolesAcceso
create table CatalogoRolesAcceso (
	IdCatalogoRolesAcceso int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoRolesAcceso primary key(IdCatalogoRolesAcceso)
);

-- Insert Catalog RolesAcceso
insert into CatalogoRolesAcceso (Nombre, Descripcion, Estatus) values 
('Eslab�n', 'Select de RolesAcceso', '1'),
('Mizar', 'Select de RolesAcceso', '1'),
('Success Factors', 'Select de RolesAcceso', '1'),
('Evaluatest', 'Select de RolesAcceso', '1'),
('Acceso a Unidad de Red', 'Select de RolesAcceso', '1');

select * from CatalogoRolesAcceso;

-- Catalog EslabonModulos
create table CatalogoEslabonModulos (
	IdCatalogoEslabonModulos int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoEslabonModulos primary key(IdCatalogoEslabonModulos)
);

-- Insert Catalog EslabonModulos
insert into CatalogoEslabonModulos (Nombre, Descripcion, Estatus) values 
('Adeudos', 'Select de EslabonModulos', '1'),
('Admin. de N�mina', 'Select de EslabonModulos', '1'),
('Admin. de RRHH', 'Select de EslabonModulos', '1'),
('Caja de Ahorro', 'Select de EslabonModulos', '1'),
('C�lculo', 'Select de EslabonModulos', '1'),
('C�lculo (PTU)', 'Select de EslabonModulos', '1'),
('C�lculo Bis', 'Select de EslabonModulos', '1'),
('Capacitaci�n', 'Select de EslabonModulos', '1'),
('Captura', 'Select de EslabonModulos', '1'),
('Cat�logos', 'Select de EslabonModulos', '1'),
('Ctrol. Actividades', 'Select de EslabonModulos', '1'),
('Ctrol. Proyecto', 'Select de EslabonModulos', '1'),
('Desarrollo RRHH', 'Select de EslabonModulos', '1'),
('Dips', 'Select de EslabonModulos', '1'),
('Empleados', 'Select de EslabonModulos', '1'),
('Empl. (Cat�logo)', 'Select de EslabonModulos', '1'),
('Fondo de Ahorro', 'Select de EslabonModulos', '1'),
('Organizaci�n', 'Select de EslabonModulos', '1'),
('Rep. Unitilizados', 'Select de EslabonModulos', '1'),
('Retroalimentaci�n', 'Select de EslabonModulos', '1'),
('Selecci�n', 'Select de EslabonModulos', '1'),
('Vacaciones', 'Select de EslabonModulos', '1'),
('Vales de Despensa', 'Select de EslabonModulos', '1'),
('IMSS', 'Select de EslabonModulos', '1'),
('Reclu. y Selecci�n', 'Select de EslabonModulos', '1'),
('Rep. & Interfa. Reci.', 'Select de EslabonModulos', '1'),
('Rep. Telecable', 'Select de EslabonModulos', '1'),
('Rep. y Proce. Desa', 'Select de EslabonModulos', '1');

select * from CatalogoEslabonModulos;
drop table CatalogoEslabonModulos;

-- Catalog MizarModulos
create table CatalogoMizarModulos (
	IdCatalogoMizarModulos int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoMizarModulos primary key(IdCatalogoMizarModulos)
);

-- Insert Catalogo MizarModulos
insert into CatalogoMizarModulos (Nombre, Descripcion, Estatus) values 
('Bancos', 'Select de MizarModulos', '1'),
('Cat�logos', 'Select de MizarModulos', '1'),
('CFDI', 'Select de MizarModulos', '1'),
('Contabilidad', 'Select de MizarModulos', '1'),
('Contabilidad Electr�nica', 'Select de MizarModulos', '1'),
('CXC', 'Select de MizarModulos', '1'),
('CXP', 'Select de MizarModulos', '1'),
('Ventas', 'Select de MizarModulos', '1');

select * from CatalogoMizarModulos;
drop table CatalogoMizarModulos;

-- Catalog LicenciasOffice
create table CatalogoLicenciasOffice (
	IdCatalogoLicenciasOffice int not null IDENTITY(1,1),
	Nombre nvarchar(75) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoLicenciasOffice primary key(IdCatalogoLicenciasOffice)
);

-- Insert Catalog LicenciasOffice
insert into CatalogoLicenciasOffice (Nombre, Descripcion, Estatus) values 
('365 Bussiness Standard (USD 15)', 'Select de LicenciasOffice', '1'),
('Office 365 E1 (USD 12)', 'Select de LicenciasOffice', '1'),
('Office 365 E3 (USD 27.6)', 'Select de LicenciasOffice', '1'),
('Power BI Pro (USD 12)', 'Select de LicenciasOffice', '1');

select * from CatalogoLicenciasOffice;
drop table CatalogoLicenciasOffice;

-- Catalog EslabonMizar
create table CatalogoEslabonMizar (
	IdCatalogoEslabonMizar int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoEslabonMizar primary key(IdCatalogoEslabonMizar)
);

-- Insert Catalog EslabonMizar
insert into CatalogoEslabonMizar (Nombre, Descripcion, Estatus) values
('Eslab�n', 'Select de EslabonMizar', '1'),
('Mizar', 'Select de EslabonMizar', '1');

select * from CatalogoEslabonMizar;
drop table CatalogoEslabonMizar;

-- Catalog OracleModulos
create table CatalogoOracleModulos (
	IdCatalogoOracleModulos int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Codigo nvarchar(30),
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoOracleModulos primary key(IdCatalogoOracleModulos)
);

-- Catalog Posicion
create table CatalogoPosicion (
	IdCatalogoPosicion int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoPosicion primary key(IdCatalogoPosicion)
);

-- Insert Posicion
insert into CatalogoPosicion (Nombre, Descripcion, Estatus) values 
('Prueba', 'Select de CatalogoNuevaPlaza', '1');

select * from CatalogoPosicion;
drop table CatalogoPosicion;

-- Catalog Admin
create table CatalogoAdmin (
	IdCatalogoAdmin int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoAdmin primary key(IdCatalogoAdmin)
);

-- Insert Admin
insert into CatalogoAdmin (Nombre, Descripcion, Estatus) values 
('Prueba', 'Select de CatalogoNuevaPlaza', '1');

select * from CatalogoAdmin;
drop table CatalogoAdmin;

-- Catalog NuevaPlaza
create table CatalogoNuevaPlaza (
	IdCatalogoNuevaPlaza int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoNuevaPlaza primary key(IdCatalogoNuevaPlaza)
);

-- Insert NuevaPlaza
insert into CatalogoNuevaPlaza (Nombre, Descripcion, Estatus) values 
('Prueba', 'Select de CatalogoNuevaPlaza', '1');

select * from CatalogoNuevaPlaza;
drop table CatalogoNuevaPlaza;

-- Insert Catalog
insert into CatalogoOracleModulos (Nombre, Codigo, Descripcion, Estatus) values
('CRM', 'B67267', 'Select de CatalogoOracleModulos', '1'),
('Finanzas', 'B69711', 'Select de CatalogoOracleModulos', '1'),
('Control de Gastos / Vi�ticos', 'B69713', 'Select de CatalogoOracleModulos', '1'),
('Compras', 'B69717', 'Select de CatalogoOracleModulos', '1'),
('Compras autoservicio', 'B69721', 'Select de CatalogoOracleModulos', '1'),
('Proyectos Management', 'B73403', 'Select de CatalogoOracleModulos', '1'),
('Task Management', 'B73405', 'Select de CatalogoOracleModulos', '1'),
('Proyectos Finanzas', 'B84628', 'Select de CatalogoOracleModulos', '1'),
('Proyectos Facturaci�n', 'B84629', 'Select de CatalogoOracleModulos', '1'),
('PBCS', 'B85698', 'Select de CatalogoOracleModulos', '1');

select * from CatalogoOracleModulos;
drop table CatalogoOracleModulos;


-- Catalog Empresas
create table CatalogoTipoSolicitud (
	IdCatalogoTipoSolicitud int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoTipoSolicitud primary key(IdCatalogoTipoSolicitud)
);

-- Insert Catalog TipoSolicitud
insert into CatalogoTipoSolicitud (Nombre, Descripcion, Estatus) values
('Solicitud Estado Equipo', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Celular', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Extension', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Correo Electronico', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Mizar & Eslabon', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Success Factor', 'Nombre Catalogo Tipo Solicitud', '1'),
('Solicitud Oracle', 'Nombre Catalogo Tipo Solicitud', '1');

select * from CatalogoTipoSolicitud;
drop table CatalogoTipoSolicitud;

-- Catalog Empresas
create table CatalogoEmpresas (
	IdCatalogoEmpresas int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_IdCatalogoEmpresas primary key(IdCatalogoEmpresas)
);

-- Insert Catalog Empresas
insert into CatalogoEmpresas (Nombre, Descripcion, Estatus) values
('Prueba', 'Select de CatalogoEmpresas', '1');

select * from CatalogoEmpresas;
drop table CatalogoEmpresas;


-- Catalog
create table Catalogo (
	Id int not null IDENTITY(1,1),
	Nombre nvarchar(30) not null,
	Descripcion nvarchar(255),
	Estatus bit,
	constraint PK_ primary key()
);

-- Insert Catalog
insert into Catalogo (Nombre, Descripcion, Estatus) values
('', 'Select de ', '1');

select * from Catalogo;
drop table Catalogo;

------------ Form Tables Solicitudes

-- Table Form EC
create table SolicitudFormEc (
	IdSolicitudFormEc int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	NombreAprobador nvarchar(70) not null,
	IdCatalogoEstadoEquipo int foreign key references CatalogoEstadoEquipo(IdCatalogoEstadoEquipo) not null,
	IdTipoAsignacion int foreign key references CatalogoTipoAsignacion(IdTipoAsignacion) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	Cargo nvarchar(30) not null,
	SoftwareInstalar nvarchar(30),
	Eslabon bit,
	Mizar bit,
	--IdCatalogoMizarModulos int foreign key references CatalogoMizarModulos(IdCatalogoMizarModulos),
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PK_IdSolicitudFormEc primary key(IdSolicitudFormEc)
);

select * from SolicitudFormEc;
drop table SolicitudFormEc;

-- Table Form Cel

create table SolicitudFormCel (
	IdSolicitudFormCel int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	NombreAprobador nvarchar(70) not null,
	IdCatalogoEstadoEquipo int foreign key references CatalogoEstadoEquipo(IdCatalogoEstadoEquipo) not null,
	IdTipoAsignacion int foreign key references CatalogoTipoAsignacion(IdTipoAsignacion) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	Cargo nvarchar(30) not null,
	PlanRenta nvarchar(30) not null,
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PKIdSolicitudFormCel primary key(IdSolicitudFormCel)
);

select * from SolicitudFormCel;
drop table SolicitudFormCel;

-- Table Form Ex

create table SolicitudFormEx (
	IdSolicitudFormEx int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	NombreAprobador nvarchar(70) not null,
	IdCatalogoEstadoEquipo int foreign key references CatalogoEstadoEquipo(IdCatalogoEstadoEquipo) not null,
	IdTipoAsignacion int foreign key references CatalogoTipoAsignacion(IdTipoAsignacion) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	Cargo nvarchar(30) not null,
	Extensiones int not null,
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PKIdSolicitudFormEx primary key(IdSolicitudFormEx)
);

select * from SolicitudFormEx;
drop table SolicitudFormEx;

-- Table Form Correo

create table SolicitudFormCorreo (
	IdSolicitudFormCorreo int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	NombreAprobador nvarchar(70) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	Lineanegocio nvarchar(30) not null,
	UEN nvarchar(30) not null,
	IdCatalogoPosicion int foreign key references CatalogoPosicion(IdCatalogoPosicion) not null,
	IdCatalogoAdmin int foreign key references CatalogoAdmin(IdCatalogoAdmin) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	Region nvarchar(30) not null,
	IdCatalogoNuevaPlaza int foreign key references CatalogoNuevaPlaza(IdCatalogoNuevaPlaza) not null,
	Reemplazo nvarchar(30) not null,
	Alta bit,
	NuevoCorreo nvarchar(30),
	idCatalogoLicenciasOffice int foreign key references CatalogoLicenciasOffice(idCatalogoLicenciasOffice),
	Baja bit,
	BajaCorreo nvarchar(30),
	Redireccionamiento bit,
	BuzonCompartido nvarchar(30),
	Nombre bit,
	CambioNombre nvarchar(30),
	Correo bit,
	CambioCorreo nvarchar(30),
	IdCatalogoDominio int foreign key references CatalogoDominio(IdCatalogoDominio) not null,
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PK_IdSolicitudFormCorreo primary key(IdSolicitudFormCorreo)
);

select * from SolicitudFormCorreo;
drop table SolicitudFormCorreo;

-- Table Form EsMz

create table SolicitudFormEsMz (
	IdSolicitudFormEsMz int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	NombreAprobador nvarchar(70) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdCatalogoPosicion int foreign key references CatalogoPosicion(IdCatalogoPosicion) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	NombreJefeDirecto nvarchar(30) not null,
	--IdCatalogoRolesAcceso int foreign key references CatalogoRolesAcceso(IdCatalogoRolesAcceso) not null,
	IdCatalogoEmpresas int foreign key references CatalogoEmpresas(IdCatalogoEmpresas) not null,
	Eslabon bit,
	EslabonGrupo nvarchar(30),
	Mizar bit,
	MizarGrupo nvarchar(30),
	--IdCatalogoMizarModulos int foreign key references CatalogoMizarModulos(IdCatalogoMizarModulos),
	RepVistas bit,
	cfdi bit,
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PK_IdSolicitudFormEsMz primary key(IdSolicitudFormEsMz)
);

select * from SolicitudFormEsMz;
drop table SolicitudFormEsMz;

-- Table Form SF

create table SolicitudFormSf (
	IdSolicitudFormSf int not null IDENTITY(1,1),
	IdCatalogoTipoSolicitud int foreign key references CatalogoTipoSolicitud(IdCatalogoTipoSolicitud) not null,
	IdAplicante int not null,
	NombreAplicante nvarchar(70) not null,
	IdArea int foreign key references CatalogoArea(IdArea) not null,
	NombreAprobador nvarchar(70) not null,
	IdEmpleado int not null,
	NombreEmpleado nvarchar(30) not null,
	ApellidoPEmpleado nvarchar(30) not null,
	ApellidoMEmpleado nvarchar(30) not null,
	MailEmpleado nvarchar(30) not null,
	IdCatalogoPosicion int foreign key references CatalogoPosicion(IdCatalogoPosicion) not null,
	IdCatalogoUbicacion int foreign key references CatalogoUbicacion(IdCatalogoUbicacion) not null,
	NombreJefeDirecto nvarchar(30) not null,
	Notas nvarchar(250),
	Fecha date not null,
	Estatus bit not null,
	constraint PK_IdSolicitudFormSf primary key(IdSolicitudFormSf)
);

select * from SolicitudFormSf;
drop table SolicitudFormSf;

-- Table Form Oracle

create table SolicitudFormOracle (
	IdSolicitudFormOracle int not null IDENTITY(1,1),
	IdEmpleado int not null,
	NombreEmpleado nvarchar(70) not null,
	CorreoEmpleado nvarchar(70) not null,
	RFC int foreign key references CatalogoArea(IdArea) not null,
	CuentaBancaria int not null,
	Clabe int not null,
	Bancos nvarchar(30) not null,
	Bu nvarchar(30) not null,
	OracleModulos nvarchar(30) not null,
	NuevoAutorizacion nvarchar(30) not null,
	SustNombre nvarchar(30) not null,
	SustCorreo nvarchar(30) not null,
	Fecha date not null,

	constraint PK_IdSolicitudFormOracle primary key(IdSolicitudFormOracle)
);

select * from SolicitudFormOracle;
drop table SolicitudFormOracle;

-- Table Form DataTable

create table SolicitudDataTable(
	IdSolicitudDataTable int not null IDENTITY(1,1),
	Formato int not null,
	Estatus bit not null,
	FechaCreacion date not null,
	FechaAutorizacion date,
	FechaCierre date not null,
	constraint PK_IdSolicitudDataTable primary key(IdSolicitudDataTable)
);

select * from SolicitudDataTable;
drop table SolicitudDataTable;


-- Table Form Solicitud_Form_Prueba

create table Solicitud_Form_Prueba (
	Id_Solicitud_Form_Prueba int not null IDENTITY(1,1),
	Id_Aplicante int not null,
	Nombre_Aplicante nvarchar(255) not null,

	constraint PK_Id_Solicitud_Form_Prueba primary key(Id_Solicitud_Form_Prueba)
);

select * from Solicitud_Form_Prueba;
truncate table Solicitud_Form_Prueba;
drop table Solicitud_Form_Prueba;

------------------ Tablas Transaccionales

-- Transaccionales FormEcMizarModulo
create table FormEcMizarModulo (
	IdFormEcMizarModulo int not null IDENTITY(1,1),
	IdSolicitudFormEc int foreign key references SolicitudFormEc(IdSolicitudFormEc),
	IdCatalogoMizarModulos int foreign key references CatalogoMizarModulos(IdCatalogoMizarModulos),
	constraint PK_IdFormEcMizarModulo primary key(IdFormEcMizarModulo)
);

select * from FormEcMizarModulo;
drop table FormEcMizarModulo;

-- Transaccionales FormEsMzMizarModulo
create table FormEsMzMizarModulo (
	IdFormEsMzMizarModulo int not null IDENTITY(1,1),
	IdSolicitudFormEsMz int foreign key references SolicitudFormEsMz(IdSolicitudFormEsMz),
	IdCatalogoMizarModulos int foreign key references CatalogoMizarModulos(IdCatalogoMizarModulos),
	constraint PK_IdFormEsMzMizarModulo primary key(IdFormEsMzMizarModulo)
);

select * from FormEsMzMizarModulo;
drop table FormEsMzMizarModulo;

-- Transaccionales FormEsMzRolesAcceso
create table FormEsMzRolesAcceso (
	IdFormEsMzRolesAcceso int not null IDENTITY(1,1),
	IdSolicitudFormEsMz int foreign key references SolicitudFormEsMz(IdSolicitudFormEsMz) not null,
	IdCatalogoRolesAcceso int foreign key references CatalogoRolesAcceso(IdCatalogoRolesAcceso) not null,
	constraint PK_IdFormEsMzRolesAcceso primary key(IdFormEsMzRolesAcceso)
);

select * from FormEsMzRolesAcceso;
drop table FormEsMzRolesAcceso;





